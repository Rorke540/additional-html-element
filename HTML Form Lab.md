# Hands-on Lab: HTML fieldset and legend Tag
<img src="Images/IDSNlogo.png" width="500" height="500"/>

<b>Effort:</b> 15 min

In this lab, we will explore a tag found within the `<form>` tag called **`<fieldset>`** tag and **`<legend>`** tag.

## Objectives

1. What is **`<fieldset>`** Tag
3. What is **`<legend>`** Tag

## HTML fieldset tag

The HTML **`<fieldset>`** tag is found within the **`<form>`** tag and is used to group related elements in an HTML form.
The fieldset element represents a set of controls in a form, optionally grouped under the same name. This element can be specially useful in large forms, where readability and ease of access can be improved with segmentation. Browsers will most likely render a frame around the grouped controls. 
The HTML **`<fieldset>`** element is found within the **`<body>`** tag.

### Syntax
`<fieldset>` Contents... `</fieldset>`


### Attribute

1. **disabled**: It specifies that the group of related form elements should be disabled.
2. **form**: It specifies that one or more forms the fieldset belong to.
3. **name**: It specifies the name for the fieldset.

### Example

In the first example we will try to create a form:

```
<!DOCTYPE html>
<html>
<body>

<h1>The fieldset element</h1>

<form>
 <fieldset>
  <label for="fname">First name:</label>
  <input type="text" id="fname" name="fname"><br><br>
  <label for="lname">Last name:</label>
  <input type="text" id="lname" name="lname"><br><br>
  <label for="email">Email:</label>
  <input type="email" id="email" name="email"><br><br>
  <label for="birthday">Birthday:</label>
  <input type="date" id="birthday" name="birthday"><br><br>
  <input type="submit" value="Submit">
 </fieldset>
 </form>

</body>
</html>
```
{: codeblock}

### Output

<img src="Images/fieldsettag.png" width="500" height="500"/>

**Note: You can edit the form as per your convenient**

## HTML legend tag

A **fieldset** can additionally have a title or name, that can be provided by **legend**.
The **`<legend>`** tag is used with the **`<fieldset>`** element as a first child to define the caption for the grouped related fields
This tag is also commonly referred to as the **`<fieldset>`** element.
By using **`<legend>`** tag with **`<form>`** elements, it is easy to understand the purpose of grouped form elements.

### Example

To understand the **`<legend>`** tag, let's add this tag to the above example and see what will be the output:

```
<!DOCTYPE html>
<html>
<body>

<h1>The fieldset element</h1>

<form>
 <fieldset>
  <legend>Personal Details:</legend>
  <label for="fname">First name:</label>
  <input type="text" id="fname" name="fname"><br><br>
  <label for="lname">Last name:</label>
  <input type="text" id="lname" name="lname"><br><br>
  <label for="email">Email:</label>
  <input type="email" id="email" name="email"><br><br>
  <label for="birthday">Birthday:</label>
  <input type="date" id="birthday" name="birthday"><br><br>
  <input type="submit" value="Submit">
 </fieldset>
 </form>

</body>
</html>
```
{: codeblock}

### Output

<img src="Images/legendtag1.png" width="500" height="500"/>




                                                                 **This Ends the Exercise**
